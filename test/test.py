#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest
import logging

from calculator.calculator import SimpleCalculator


class AdditionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()
          

    def test_addition(self):
        result = self.calculator.sum(1, "2")
        self.assertEqual(result, "ERROR")
        logging.warning('attention')



class SubtractionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()


    def test_subtraction(self):
        result = self.calculator.subtract(1, "2")
        self.assertEqual(result, "ERROR")
        logging.warning('attention')



class MultiplicationTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()


    def test_multiplication(self):
        result = self.calculator.subtract(1, "2")
        self.assertEqual(result, "ERROR")
        logging.warning('attention')


class DivisionTestSuite(unittest.TestCase):
    def setUp(self):
        """ Executed before every test case """
        self.calculator = SimpleCalculator()


    def test_division_integer_string(self):
        result = self.calculator.divide(1, "2")
        self.assertEqual(result, "ERROR")
        logging.warning('attention')



if __name__ == "__main__":
    unittest.main()

