#!/usr/bin/env python
# coding: utf_8

"""
Module simplecalculator
"""


class SimpleCalculator:
    """ 
	Classe permettant de faire la somme, la soustraction, la multiplication et division
	
	"""

    @staticmethod
    def sum(var_a, var_b):
        """
		Addition de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la somme
		"""
        if isinstance(var_a, int) and isinstance(var_b, int):
            return var_a + var_b
        else:
            return "ERROR"

    @staticmethod
    def substract(var_a, var_b):
        """
		Soustraction de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la soustraction
		"""
        if isinstance(var_a, int) and isinstance(var_b, int):
            return var_a - var_b
        else:
            return "ERROR"

    @staticmethod
    def multiply(var_a, var_b):
        """
		Multiplication de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la multiplication
		"""
        if isinstance(var_a, int) and isinstance(var_b, int):
            return var_a * var_b
        else:
            return "ERROR"

    @staticmethod
    def divide(var_a, var_b):
        """
		Division de deux nombres
		Entrees: var_a et var_b deux entiers
		Return: un entier correspondant à la division
		"""

        if isinstance(var_a, int) and isinstance(var_b, int):
            if var_b == 0:
                return "Pas de division par 0"
            else:
                return var_a / var_b
        else:
            return "ERROR"


